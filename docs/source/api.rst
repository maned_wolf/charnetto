.. _api:


Developer Interface
===================

.. module:: charnetto

This part of the documentation covers the public interface of charnetto.

Book
----

.. autofunction:: extract_flair_df
.. autofunction:: extract_spacy_df
.. autofunction:: unify_tags
.. autofunction:: extract_manual_df

Movie
-----

.. autofunction:: extract_movie_df

Characters
----------

.. autofunction:: concatenate_parents
.. autofunction:: write_charlist
.. autofunction:: highlight

Network
-------
.. autofunction:: load_char_list
.. autofunction:: map_names
.. autofunction:: find_pairs
.. autofunction:: create_charnet
